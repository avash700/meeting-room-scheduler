'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert('users', [
      {
        firstName: "admin fname",
        lastName: "admin lname",
        email: "admin@mail.com",
        password: "$2b$10$4xDIp1mtpmHPVGgWleHCR.kavKSc/yUJlXaRsEvuR8eS7eekSQeHq",
        role_id:1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: "emp fname",
        lastName: "emp lname",
        email: "employee1@mail.com",
        password: "$2b$10$.d68gxQxTUWPP/yHE8bCfuBcqpjl6piVrfvwIkqE090.YjfFqRpta",
        role_id:2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: "emp fname",
        lastName: "emp lname",
        email: "employee2@mail.com",
        password: "$2b$10$.d68gxQxTUWPP/yHE8bCfuBcqpjl6piVrfvwIkqE090.YjfFqRpta",
        role_id:2,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, {});
  }
};
