const ApiError = require("../utils/errors.util")
const { Amenities, Rooms } = require("../models")

const getAmenities = async () => {
    const amenities = await Amenities.findAll()
    return amenities
}

const getAmenity = async (id) => {
    const amenity = await Amenities.findByPk(id)
    if (!amenity) throw ApiError.notFound("amenity not found")
    return amenity
}

const createAminity = async (reqObj) => {
    const aminityInDB = await Amenities.findOne({ where: { amenity_name: reqObj.amenity_name } })
    if (aminityInDB) throw ApiError.alreadyExists("amenity already exists")
    await Amenities.create(reqObj)
    return 1
}

const deleteAmenity = async (id) => {
    const amenity = await Amenities.findByPk(id, {
        include: [{
            model: Rooms
        }]
    })
    if (!amenity) throw ApiError.notFound("amenity not found")
    if(amenity.Rooms.length > 0) throw ApiError.badRequest("cannot delete an amenity in use")
    await Amenities.destroy({ where: { id } })
}

const updateAmenity = async (id, reqObj) => {
    const amenity = await Amenities.findByPk(id)
    if (!amenity) throw ApiError.notFound("amenity not found")
    const amenityInDB = await Amenities.findOne({ where: { amenity_name: reqObj.amenity_name } })
    if (amenityInDB) throw ApiError.alreadyExists("amenity already exists")
    await Amenities.update({ ...reqObj }, { where: { id } })
    return 1
}

module.exports = {
    getAmenities,
    getAmenity,
    createAminity,
    deleteAmenity,
    updateAmenity
}