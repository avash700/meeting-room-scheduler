const ApiError = require("../utils/errors.util")
const { Users, Roles, MeetingsUsers, Meetings } = require("../models")
const { hashPassword } = require("../utils/bcrypt.util")

const getUsers = async () => {
    const users = await Users.findAll({
        attributes: {
            exclude: ["password"]
        },
        include: [
            {
                model: Roles,
                attributes: ["id", "role_type"],
            }
        ]
    });
    return users
}

const getUser = async (id) => {
    const user = await Users.findByPk(id, {
        attributes: {
            exclude: ["password"]
        },
        include: [
            {
                model: Roles,
                attributes: ["id", "role_type"],
            }
        ]
    })

    if (!user) throw ApiError.badRequest("user not found")
    return user
}

const createUser = async (reqObj) => {
    const oldUser = await Users.findOne({ where: { email: reqObj.email } })
    if (oldUser) throw ApiError.alreadyExists("user with the email already exists")

    const role = await Roles.findByPk(reqObj.role_id)
    if (!role) throw ApiError.notFound("role with the id not found")
    const plainPassword = reqObj.password
    reqObj.password = hashPassword(reqObj.password);
    await Users.create(reqObj);

    return {
        email: reqObj.email,
        password: plainPassword,
        role: role.role_type
    }
}

const deleteUser = async (id) => {
    const user = await Users.findByPk(id)
    if (!user) throw ApiError.notFound("user not found")
    const userHost = await Meetings.findAll({ where: { host_id: id } })
    if (userHost.length > 0) throw ApiError.badRequest("cannot delete a user who is hosting a meeting")
    const userAttendee = await MeetingsUsers.findAll({ where: { user_id: id } })
    if (userAttendee.length > 0) throw ApiError.badRequest("cannot delete a user who is attending a meeting")
    await Users.destroy({ where: { id } })
    return 1
}

const updateUser = async (userId, reqObj) => {
    const oldUser = await Users.findOne({ where: { id: userId } })
    if (!oldUser) throw ApiError.notFound("user not found")
    if (oldUser.id !== userId) throw ApiError.notAuthorized("not authorized to perform this action")

    if (reqObj.email) {
        const emailInDB = await Users.findOne({ where: { email: reqObj.email } })
        if (emailInDB) throw ApiError.alreadyExists("email already exists")
    }

    await Users.update({ ...reqObj }, { where: { id: userId } })
    return 1
}

module.exports = {
    getUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
}