const { DateTime } = require("luxon")
const ApiError = require("../utils/errors.util")
const { Rooms, Amenities, RoomsAmenities, Meetings } = require("../models")
const { Op, where } = require("sequelize")
const { isOverLapWithGap } = require("../utils/date.util")

const getRooms = async () => {
    const rooms = await Rooms.findAll({
        include: [{
            model: Amenities
        }]
    })
    return rooms
}

const getRoomsByAmenities = async (options) => {
    const { amenities } = options
    const rooms = await Rooms.findAll({
        include: [{
            required: true,
            model: Amenities,
            through: {
                where: {
                    amenity_id: {
                        [Op.or]: amenities
                    }
                }
            }
        }]
    })

    return rooms

}

const getRoom = async (id) => {
    const room = await Rooms.findByPk(id, {
        include: [{
            model: Amenities
        }]
    })
    if (!room) throw ApiError.notFound("room not found")
    return room
}

const createRoom = async (reqObj) => {
    const roomInDB = await Rooms.findOne({ where: { room_name: reqObj.room_name } })
    if (roomInDB) throw ApiError.alreadyExists("room name already exists")

    // check if the amenity exists, return api error for the first amenity not found
    for (const amenity of reqObj.amenities) {
        const amenityInDb = await Amenities.findByPk(amenity)
        if (!amenityInDb) throw ApiError.notFound(`amenity with id: ${amenity} doesn't exist.`)
    }

    const newRoom = await Rooms.create(reqObj)
    const amenitiesToAdd = reqObj.amenities.map(amenity => ({
        amenity_id: amenity,
        room_id: newRoom.id,
    }))

    await RoomsAmenities.bulkCreate(amenitiesToAdd, {
        fields: ["amenity_id", "room_id"],
    })

    return 1
}

const deleteRoom = async (id) => {
    const room = await Rooms.findByPk(id, {
        include: [{
            model: Meetings
        }]
    })
    if (!room) throw ApiError.notFound("room not found")
    if (room.Meetings.length > 0) throw ApiError.badRequest("cannot delete a room booked for meeting")
    await RoomsAmenities.destroy({ where: { room_id: id } })
    await Rooms.destroy({ where: { id } })
}

const updateRoom = async (id, reqObj) => {
    const room = await getRoom(id)
    if (!room) throw ApiError.notFound("room not found")

    const oldAmenities = room.Amenities.map(item => item.id)

    let room_name = reqObj.room_name || room.room_name
    let capacity = reqObj.capacity || room.capacity
    let amenities = reqObj.amenities || oldAmenities

    if (reqObj.room_name) {
        const roomInDB = await Rooms.findOne({ where: { room_name: reqObj.room_name } })
        if (roomInDB) throw ApiError.alreadyExists("room name already exists")
    }

    for (const amenity of amenities) {
        const amenityInDb = await Amenities.findByPk(amenity)
        if (!amenityInDb) throw ApiError.notFound(`amenity with id: ${amenity} doesn't exist.`)
    }

    const newObj = {
        room_name,
        capacity,
        amenities
    }

    await Rooms.update({ ...newObj }, { where: { id } })
    if (reqObj.amenities) {
        // clear all past amenities
        await RoomsAmenities.destroy({ where: { amenity_id: oldAmenities, room_id: id } })
        const toAdd = amenities.map(amenity => ({
            amenity_id: amenity,
            room_id: id,
        }))

        await RoomsAmenities.bulkCreate(toAdd, {
            fields: ["amenity_id", "room_id"],
        })
        //add new amenities
    }
    return 1
}

const addAmenitiesToRoom = async (id, amenitiesToAdd) => {
    const room = await Rooms.findByPk(id)
    if (!room) throw ApiError.notFound("room not found")

    for (const amenity of amenitiesToAdd) {
        const amenityInDb = await Amenities.findByPk(amenity)
        if (!amenityInDb) throw ApiError.notFound(`amenity with id: ${amenity} doesn't exist.`)

        // check if amenity is already added to the room
        const amenityInRoom = await RoomsAmenities.findOne({ where: { amenity_id: amenity, room_id: id } })

        if (amenityInRoom) throw ApiError.alreadyExists(`Amenity with id: ${amenity} already exists in the room.`)
    }

    const toAdd = amenitiesToAdd.map(amenity => ({
        amenity_id: amenity,
        room_id: id,
    }))

    await RoomsAmenities.bulkCreate(toAdd, {
        fields: ["amenity_id", "room_id"],
    })

    return 1
}

const removeAmenitiesFromRoom = async (id, amenitiesToRemove) => {
    const room = await Rooms.findByPk(id, {
        include: [{
            model: Amenities
        }]
    })
    if (!room) throw ApiError.notFound("room not found")

    // check if the requested amenities are in the room
    for (const amenity of amenitiesToRemove) {
        const amenityExists = room.Amenities.find(am => am.id === amenity)
        if (!amenityExists) throw ApiError.notFound(`amenity with id: ${amenity} is not in the room`)
    }

    await RoomsAmenities.destroy({ where: { amenity_id: amenitiesToRemove, room_id: id } })

    return 1
}

const checkAvailability = async (id, reqObj) => {
    const end = DateTime.fromISO(reqObj.end)
    const start = DateTime.fromISO(reqObj.start)
    const now = DateTime.now()

    if (start <= now) throw ApiError.badRequest("availability cannot be checked for past date")
    if (end <= start) throw ApiError.badRequest("end time must be greater than start time")

    const roomMeetings = await Rooms.findByPk(id, {
        include: [{
            model: Meetings,
            required: false,
            where: {
                start: {
                    [Op.gte]: DateTime.now()
                }
            }
        }]
    })
    if (!roomMeetings) throw ApiError.notFound("room not found")

    const overLappedWith = []
    roomMeetings.Meetings.forEach(meeting => {
        // gap in minutes
        const gap = 5
        const dbStart = new Date(meeting.start).toISOString()
        const dbEnd = new Date(meeting.end).toISOString()

        // ! console.log(DateTime.fromISO(new Date(meeting.start).toISOString()).toString())
        const meetbool = isOverLapWithGap(dbStart, dbEnd, start.toISO(), end.toISO(), gap)
        if (meetbool) {
            overLappedWith.push(`overlaps with meeting id ${meeting.id}`)
        }
    });
    if (overLappedWith.length === 0) {
        return { available: true, message: "room is available for booking in the given slot" }
    } else {
        return { available: false, message: "not availabe for booking in the time slot", overLappedWith }
    }

}

module.exports = {
    getRooms,
    getRoom,
    createRoom,
    deleteRoom,
    updateRoom,
    addAmenitiesToRoom,
    removeAmenitiesFromRoom,
    checkAvailability,
    getRoomsByAmenities
}