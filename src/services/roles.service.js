const ApiError = require("../utils/errors.util")
const { Roles, Users } = require("../models")

const getRoles = async () => {
    const roles = await Roles.findAll()
    return roles
}

const getRole = async (id) => {
    const role = await Roles.findByPk(id)
    if (!role) throw ApiError.notFound("role not found")
    return role
}

const createRole = async (reqObj) => {
    const roleInDB = await Roles.findOne({ where: { role_type: reqObj.role_type } })
    if (roleInDB) throw ApiError.alreadyExists("role already exists")
    await Roles.create(reqObj)
    return 1
}

const deleteRole = async (id) => {
    const role = await Roles.findByPk(id, {
        include: [{
            model: Users
        }]
    })
    if (!role) throw ApiError.notFound("role not found")
    if(role.Users.length > 0) throw ApiError.badRequest("cannot delete a role in use")
    await Roles.destroy({ where: { id } })
}

const updateRole = async (id, reqObj) => {
    const role = await Roles.findByPk(id, {
        include: [{
            model: Users
        }]
    })
    if (!role) throw ApiError.notFound("role not found")
    if(role.Users.length > 0) throw ApiError.badRequest("cannot update a role in use")
    const roleInDB = await Roles.findOne({ where: { role_type: reqObj.role_type } })
    if (roleInDB) throw ApiError.alreadyExists("role already exists")
    await Roles.update({ ...reqObj }, { where: { id } })
    return 1
}

module.exports = {
    getRoles,
    getRole,
    createRole,
    deleteRole,
    updateRole
}