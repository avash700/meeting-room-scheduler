const ApiError = require("../utils/errors.util")
const { Meetings, Users, Rooms, MeetingsUsers } = require("../models")
const { Op } = require("sequelize")
const { DateTime, Settings } = require("luxon")
const { isOverLapWithGap } = require("../utils/date.util")
const { pick } = require("../utils/object.util")
const { transporter, meetingOption } = require("../utils/mailer.util")

Settings.throwOnInvalid = true

const getMeetings = async (options) => {
    // let { year, month, day } = pick(options, ["year", "month", "day"])
    let { date, limit, page, room } = pick(options, ["date", "limit", "page", "room"])

    const newLimit = parseInt(limit) || 10
    const newPage = parseInt(page) || 1

    const queryOptions = {}
    if (date) {
        const splitDate = date.split('/')

        let startDate = {
            year: splitDate[0],
            month: 1,
            day: 1,
            hour: 00,
            minute: 00,
            second: 00
        }
        let endDate = {
            year: splitDate[0],
            month: 12,
            day: 31,
            hour: 23,
            minute: 59,
            second: 59
        }

        if (splitDate[1]) {
            startDate.month = splitDate[1]
            endDate.month = splitDate[1]
            endDate.day = new Date(splitDate[0], splitDate[1], 0).getDate()
        }

        if (splitDate[2]) {
            startDate.day = splitDate[2]
            endDate.day = splitDate[2]
        }
        let start, end
        try {
            start = DateTime.fromObject({ ...startDate }).toISO()
            end = DateTime.fromObject({ ...endDate }).toISO()
            queryOptions.where = {
                ...queryOptions.where,
                start: {
                    [Op.between]: [start, end]
                }
            }
        } catch (e) {
            throw ApiError.badRequest(e.message)
        }
    }

    if (room) {
        queryOptions.where = {
            ...queryOptions.where,
            room_id: room
        }
    }

    // return meetings
    return await Meetings.findAll({
        include: [{
            model: Users,
            as: 'host',
            attributes: ["id", "firstName", "lastName", "email"]
        }, {
            model: Rooms,
            attributes: ["id", "room_name"]
        }],
        order: [["start", "ASC"]],
        offset: (newPage - 1) * newLimit,
        limit: newLimit,
        where: { ...queryOptions.where }
    })
}

const getMeeting = async (id) => {
    const meeting = await Meetings.findByPk(id, {
        include: [{
            model: Users,
            as: 'host',
            attributes: ["id", "firstName", "lastName", "email"]
        }, {
            model: Rooms,
            attributes: ["id", "room_name"]
        }, {
            model: Users,
            as: 'meeting_attendees',
            attributes: ["id", "firstName", "lastName", "email"]
        }]
    })
    if (!meeting) throw ApiError.notFound("meeting not found")
    return meeting
}

const createMeeting = async (reqObj) => {

    const room = await Rooms.findByPk(reqObj.room_id)
    if (!room) throw ApiError.notFound("room not found")
    const now = DateTime.now()
    const start = DateTime.fromISO(reqObj.start)
    const end = DateTime.fromISO(reqObj.end)
    //! start of meeting should be atleast 30 minutes ahead from current time
    const start_now_diff = start.diff(now, 'minutes').toObject().minutes
    if (start_now_diff < 30) throw ApiError.badRequest("start time must be 30 minutes ahead of current time")
    //! end time must be greater than start time
    if (end <= start) throw ApiError.badRequest("end time must be greater than start time")

    //! check if the room is already taken in the given time period
    // get room and meetings for meetings in time more than current time
    const roomMeetings = await Rooms.findOne({
        include: [{
            model: Meetings,
            required: false,
            where: {
                start: {
                    [Op.gte]: DateTime.now()
                }
            }
        }],
        where: { id: reqObj.room_id }
    })

    // console.log(JSON.stringify(roomMeetings.Meetings,null,4))
    roomMeetings.Meetings.forEach(meeting => {
        // gap in minutes
        const gap = 5
        const dbStart = new Date(meeting.start).toISOString()
        const dbEnd = new Date(meeting.end).toISOString()

        // ! console.log(DateTime.fromISO(new Date(meeting.start).toISOString()).toString())
        const meetbool = isOverLapWithGap(dbStart, dbEnd, start.toISO(), end.toISO(), gap)
        if (meetbool) {
            throw ApiError.badRequest("room booked in the time slot. 5 minute gap between meetings required")
        }
    });

    // check if the user exists, return api error for the first user not found
    for (const attendee of reqObj.attendees) {
        if (reqObj.host_id === attendee) throw ApiError.badRequest('Cannot add yourself to the meeting')
        const userInDb = await Users.findByPk(attendee, {
            attributes: {
                exclude: ['password']
            }
        })
        if (!userInDb) throw ApiError.notFound(`User with id: ${attendee} doesn't exist.`)
    }

    const newMeeting = await Meetings.create(reqObj)
    const usersToAdd = reqObj.attendees.map(user => ({
        user_id: user,
        meeting_id: newMeeting.id,
    }))

    await MeetingsUsers.bulkCreate(usersToAdd, {
        fields: ["user_id", "meeting_id"],
    })

    const newMeetingData = await getMeeting(newMeeting.id)

    const attendeeMail = []
    newMeetingData.meeting_attendees.forEach(data => {
        attendeeMail.push(data.email)
    });

    return {
        attendeeMail,
        name: newMeetingData.name,
        description: newMeetingData.description,
        hostFirstName: newMeetingData.host.firstName,
        room: newMeetingData.Room.room_name,
        hostLastName: newMeetingData.host.lastName,
        meetingStart: new Date(newMeetingData.start).toLocaleString(),
        meetingEnd: new Date(newMeetingData.end).toLocaleString()
    }
}

const deleteMeeting = async (id, userId) => {
    const meeting = await getMeeting(id)
    if (!meeting) throw ApiError.notFound("meeting not found")
    if (meeting.host_id != userId) throw ApiError.notAuthorized("not authorized to perform this action")
    await MeetingsUsers.destroy({ where: { meeting_id: id } })
    await Meetings.destroy({ where: { id } })
    const attendeeMail = []
    meeting.meeting_attendees.forEach(data => {
        attendeeMail.push(data.email)
    });

    return {
        attendeeMail,
        name: meeting.name,
        description: meeting.description,
        hostFirstName: meeting.host.firstName,
        room: meeting.Room.room_name,
        hostLastName: meeting.host.lastName,
        meetingStart: new Date(meeting.start).toLocaleString(),
        meetingEnd: new Date(meeting.end).toLocaleString()
    }
}

const updateMeeting = async (id, userId, reqObj) => {
    const meeting = await getMeeting(id)
    if (!meeting) throw ApiError.notFound("meeting not found")
    if (meeting.host_id != userId) throw ApiError.notAuthorized("not authorized to perform this action")

    let startTime = meeting.start.toISOString()
    if (DateTime.fromISO(startTime) <= DateTime.now()) throw ApiError.badRequest("cannot update a past meeting")

    const oldAttendees = meeting.meeting_attendees.map(item => item.id)

    let endTime = meeting.end.toISOString()
    let room_id = reqObj.room_id || meeting.room_id
    let name = reqObj.name || meeting.name
    let description = reqObj.description || meeting.description
    let attendees = reqObj.attendees || oldAttendees
    if (reqObj.start) {
        startTime = DateTime.fromISO(reqObj.start).toISO()
    }

    if (reqObj.end) {
        endTime = DateTime.fromISO(reqObj.end).toISO()
    }

    const room = await Rooms.findByPk(room_id)
    if (!room) throw ApiError.notFound("room not found")
    const now = DateTime.now()
    const start = DateTime.fromISO(startTime)
    const end = DateTime.fromISO(endTime)
    // //! start of meeting should be atleast 30 minutes ahead from current time
    const start_now_diff = start.diff(now, 'minutes').toObject().minutes
    if (start_now_diff < 30) throw ApiError.badRequest("start time must be 30 minutes ahead of current time")
    // //! end time must be greater than start time
    if (end <= start) throw ApiError.badRequest("end time must be greater than start time")

    // //! check if the room is already taken in the given time period
    // get room and meetings for meetings in time more than current time
    const roomMeetings = await Rooms.findOne({
        include: [{
            model: Meetings,
            required: false,
            where: {
                start: {
                    [Op.gte]: DateTime.now()
                }
            }
        }],
        where: { id: room_id }
    })

    // console.log(JSON.stringify())
    const filteredMeetings = roomMeetings.Meetings.filter(meeting => meeting.id != id)

    filteredMeetings.forEach(meeting => {
        // gap in minutes
        const gap = 5
        const dbStart = new Date(meeting.start).toISOString()
        const dbEnd = new Date(meeting.end).toISOString()

        // ! console.log(DateTime.fromISO(new Date(meeting.start).toISOString()).toString())
        const meetbool = isOverLapWithGap(dbStart, dbEnd, start, end, gap)
        if (meetbool) {
            throw ApiError.badRequest("room booked in the time slot. 5 minute gap between meetings required")
        }
    });

    for (const attendee of attendees) {
        if (userId === attendee) throw ApiError.badRequest('Cannot add yourself to the meeting')
        // check if user exists in the database
        const userInDb = await Users.findByPk(attendee, {
            attributes: {
                exclude: ['password']
            }
        })
        if (!userInDb) throw ApiError.notFound(`User with id: ${attendee} doesn't exist.`)

    }

    const newObj = {
        room_id,
        start,
        end,
        name,
        description
    }

    await Meetings.update({ ...newObj }, { where: { id } })

    if (reqObj.attendees) {
        // clear all past attendees
        await MeetingsUsers.destroy({ where: { user_id: oldAttendees, meeting_id: id } })
        // add new attendees
        const toAdd = attendees.map(attendee => ({
            user_id: attendee,
            meeting_id: id,
        }))

        await MeetingsUsers.bulkCreate(toAdd, {
            fields: ["user_id", "meeting_id"],
        })
    }

    const newMeetingData = await getMeeting(id)

    const attendeeMail = []
    const removedMail = []
    const addedMail = []

    newMeetingData.meeting_attendees.forEach(data => {
        attendeeMail.push(data.email)
    });

    const removedUsers = oldAttendees.filter(x => !attendees.includes(x));
    const addedUsers = attendees.filter(x => !oldAttendees.includes(x));

    for (const user of removedUsers) {
        const userInDb = await Users.findByPk(user)
        removedMail.push(userInDb.email)
    }

    for (const user of addedUsers) {
        const userInDb = await Users.findByPk(user)
        addedMail.push(userInDb.email)
    }

    return {
        attendeeMail,
        removedMail,
        addedMail,
        name: newMeetingData.name,
        description: newMeetingData.description,
        hostFirstName: newMeetingData.host.firstName,
        room: newMeetingData.Room.room_name,
        hostLastName: newMeetingData.host.lastName,
        meetingStart: new Date(newMeetingData.start).toLocaleString(),
        meetingEnd: new Date(newMeetingData.end).toLocaleString()
    }
}

const addUsersToMeeting = async (id, userId, usersToAdd) => {
    const meeting = await getMeeting(id)
    if (!meeting) throw ApiError.notFound("meeting not found")
    if (meeting.host_id != userId) throw ApiError.notAuthorized("not authorized to perform this action")

    let startTime = meeting.start.toISOString()
    if (DateTime.fromISO(startTime) <= DateTime.now()) throw ApiError.badRequest("cannot add users to a past meeting")


    const attendeeMail = []
    for (const user of usersToAdd) {
        if (userId === user) throw ApiError.badRequest('Cannot add yourself to the meeting')
        // check if user exists in the database
        const userInDb = await Users.findByPk(user, {
            attributes: {
                exclude: ['password']
            }
        })
        if (!userInDb) throw ApiError.notFound(`User with id: ${user} doesn't exist.`)
        attendeeMail.push(userInDb.email)
        // check if user is already invited to the meeting
        const userInMeeting = await MeetingsUsers.findOne({ where: { user_id: user, meeting_id: id } })

        if (userInMeeting) throw ApiError.badRequest(`User with id: ${user} already exists in the meeting.`)
    }

    const toAdd = usersToAdd.map(user => ({
        user_id: user,
        meeting_id: id,
    }))

    await MeetingsUsers.bulkCreate(toAdd, {
        fields: ["user_id", "meeting_id"],
    })

    const newMeetingData = await getMeeting(id)
    return {
        attendeeMail,
        name: newMeetingData.name,
        description: newMeetingData.description,
        hostFirstName: newMeetingData.host.firstName,
        room: newMeetingData.Room.room_name,
        hostLastName: newMeetingData.host.lastName,
        meetingStart: new Date(newMeetingData.start).toLocaleString(),
        meetingEnd: new Date(newMeetingData.end).toLocaleString()
    }
}

const removeUsersFromMeeting = async (id, userId, usersToRemove) => {
    const meeting = await getMeeting(id)
    if (!meeting) throw ApiError.notFound("meeting not found")
    if (meeting.host_id !== userId) throw ApiError.notAuthorized("not authorized to perform this action")

    let startTime = meeting.start.toISOString()
    if (DateTime.fromISO(startTime) <= DateTime.now()) throw ApiError.badRequest("cannot remove users from a past meeting")

    const attendeeMail = []

    // check if the requested users are in the meeting
    for (const user of usersToRemove) {
        const userExists = meeting.meeting_attendees.find(attendee => attendee.id === user)
        if (!userExists) throw ApiError.notFound(`user with id: ${user} is not in the meeting`)
        attendeeMail.push(userExists.email)
    }

    await MeetingsUsers.destroy({ where: { user_id: usersToRemove, meeting_id: id } })

    return {
        attendeeMail,
        name: meeting.name,
        description: meeting.description,
        hostFirstName: meeting.host.firstName,
        room: meeting.Room.room_name,
        hostLastName: meeting.host.lastName,
        meetingStart: new Date(meeting.start).toLocaleString(),
        meetingEnd: new Date(meeting.end).toLocaleString()
    }
}

const getInvitedUserMeetings = async (options, userId) => {
    let { date, limit, page, room } = pick(options, ["date", "limit", "page", "room"])

    const newLimit = parseInt(limit) || 10
    const newPage = parseInt(page) || 1

    const queryOptions = {}

    // if date is given get all the meetings for given date
    // else show upcoming meetings from the current time 
    if (date) {
        const splitDate = date.split('/')

        let startDate = {
            year: splitDate[0],
            month: 1,
            day: 1,
            hour: 00,
            minute: 00,
            second: 00
        }
        let endDate = {
            year: splitDate[0],
            month: 12,
            day: 31,
            hour: 23,
            minute: 59,
            second: 59
        }

        if (splitDate[1]) {
            startDate.month = splitDate[1]
            endDate.month = splitDate[1]
            endDate.day = new Date(splitDate[0], splitDate[1], 0).getDate()
        }

        if (splitDate[2]) {
            startDate.day = splitDate[2]
            endDate.day = splitDate[2]
        }
        let start, end
        try {
            start = DateTime.fromObject({ ...startDate }).toISO()
            end = DateTime.fromObject({ ...endDate }).toISO()
            queryOptions.where = {
                ...queryOptions.where,
                start: {
                    [Op.between]: [start, end]
                }
            }
        } catch (e) {
            throw ApiError.badRequest(e.message)
        }
    } else {
        queryOptions.where = {
            start: {
                [Op.gte]: DateTime.now()
            }
        }
    }

    if (room) {
        queryOptions.where = {
            ...queryOptions.where,
            room_id: room
        }
    }

    const meetings = await MeetingsUsers.findAll({
        include: [{
            model: Meetings,
            include: [{
                model: Users,
                as: 'host',
                attributes: {
                    exclude: ["password"]
                }
            }, {
                model: Users,
                as: 'meeting_attendees',
                attributes: {
                    exclude: ["password"]
                }
            }, {
                model: Rooms
            }],
            order: [["start", "ASC"]],
            where: { ...queryOptions.where }
        }],
        where: { user_id: userId },
        offset: (newPage - 1) * newLimit,
        limit: newLimit
    })

    // const meetings = await Meetings.findAll({
    //     include: [{
    //         model: Users,
    //         as: 'host',
    //         attributes: ["id", "firstName", "lastName", "email"]
    //     }, {
    //         model: Rooms,
    //         attributes: ["id", "room_name"]
    //     }, {
    //         model: Users,
    //         required: true,
    //         as: 'meeting_attendees',
    //         through: {
    //             where: {
    //                 user_id: userId
    //             }
    //         },
    //         attributes: ["id", "firstName", "lastName", "email"],
    //     }],
    //     where: {
    //         ...queryOptions.where
    //     },
    //     order: [["start", "ASC"]],
    //     offset: (newPage - 1) * newLimit,
    //     limit: newLimit
    // })

    return meetings
}

const getHostedUserMeetings = async (options, userId) => {
    let { date, limit, page, room } = pick(options, ["date", "limit", "page", "room"])

    const newLimit = parseInt(limit) || 10
    const newPage = parseInt(page) || 1

    const queryOptions = {}

    // if date is given get all the meetings for given date
    // else show upcoming meetings from the current time 
    if (date) {
        const splitDate = date.split('/')

        let startDate = {
            year: splitDate[0],
            month: 1,
            day: 1,
            hour: 00,
            minute: 00,
            second: 00
        }
        let endDate = {
            year: splitDate[0],
            month: 12,
            day: 31,
            hour: 23,
            minute: 59,
            second: 59
        }

        if (splitDate[1]) {
            startDate.month = splitDate[1]
            endDate.month = splitDate[1]
            endDate.day = new Date(splitDate[0], splitDate[1], 0).getDate()
        }

        if (splitDate[2]) {
            startDate.day = splitDate[2]
            endDate.day = splitDate[2]
        }
        let start, end
        try {
            start = DateTime.fromObject({ ...startDate }).toISO()
            end = DateTime.fromObject({ ...endDate }).toISO()
            queryOptions.where = {
                start: {
                    [Op.between]: [start, end]
                }
            }
        } catch (e) {
            throw ApiError.badRequest(e.message)
        }
    } else {
        queryOptions.where = {
            ...queryOptions.where,
            start: {
                [Op.gte]: DateTime.now()
            }
        }
    }

    if (room) {
        queryOptions.where = {
            ...queryOptions.where,
            room_id: room
        }
    }


    const meetings = await Meetings.findAll({
        include: [{
            model: Users,
            as: 'host',
            attributes: ["id", "firstName", "lastName", "email"]
        }, {
            model: Rooms,
            attributes: ["id", "room_name"]
        }, {
            model: Users,
            as: 'meeting_attendees',
            attributes: ["id", "firstName", "lastName", "email"],
        }],
        where: {
            ...queryOptions.where,
            host_id: userId
        },
        order: [["start", "ASC"]],
        offset: (newPage - 1) * newLimit,
        limit: newLimit
    })

    return meetings
}

const notifytUpcomingMeetings = async (date) => {
    const meetings = await Meetings.findAll({
        include: [{
            model: Users,
            as: 'host',
            attributes: ["id", "firstName", "lastName", "email"]
        }, {
            model: Rooms,
            attributes: ["id", "room_name"]
        }, {
            model: Users,
            as: 'meeting_attendees',
            attributes: ["id", "firstName", "lastName", "email"],
        }],
        where: {
            start: {
                [Op.gte]: date
            }
        }
    })

    for (const meeting of meetings) {
        const timeEarly = DateTime.fromISO(meeting.start.toISOString()).minus({ minutes: 15 })
        if (date >= timeEarly) {
            const attendeeMail = []
            meeting.meeting_attendees.forEach(attendee => {
                attendeeMail.push(attendee.email)
            });
            attendeeMail.push(meeting.host.email)
            const mailData = {
                attendeeMail,
                name: meeting.name,
                description: meeting.description,
                hostFirstName: meeting.host.firstName,
                room: meeting.Room.room_name,
                hostLastName: meeting.host.lastName,
                meetingStart: new Date(meeting.start).toLocaleString(),
                meetingEnd: new Date(meeting.end).toLocaleString()
            }

            await transporter.sendMail(meetingOption("meeting starting in 15 minutes", mailData))
        }
    }
}

const getTodaysUpcomingMeetings = async (options, userId) => {
    // let { limit, page, room } = pick(options, ["limit", "page", "room"])
    let { room } = pick(options, ["room"])

    // const newLimit = parseInt(limit) || 10
    // const newPage = parseInt(page) || 1

    const queryOptions = {}

    if (room) {
        queryOptions.where = {
            ...queryOptions.where,
            room_id: room
        }
    }


    const meetings = await Meetings.findAll({
        where: {
            ...queryOptions.where,
            [Op.or]: [
                { '$host.id$': userId },
                { '$meeting_attendees.id$': userId }
            ],
            start: {
                [Op.gte]: DateTime.now()
            }
        },
        include: [{
            model: Users,
            as: 'host',
            attributes: {
                exclude: ['password']
            }
        }, {
            model: Users,
            as: 'meeting_attendees',
            through: { attributes: [] },
            attributes: {
                exclude: ['password']
            }
        }],
        order: [["start", "ASC"]],
        // offset: 10,
        // limit: 1
    })

    return meetings
}


module.exports = {
    getMeetings,
    getMeeting,
    createMeeting,
    deleteMeeting,
    updateMeeting,
    addUsersToMeeting,
    removeUsersFromMeeting,
    getInvitedUserMeetings,
    getHostedUserMeetings,
    getTodaysUpcomingMeetings,
    notifytUpcomingMeetings
}