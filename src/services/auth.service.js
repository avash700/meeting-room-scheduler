const { Users, Roles, ResetOtp } = require("../models")
const { generateToken } = require("../utils/jwt.util")
const { comparePassword, hashPassword } = require("../utils/bcrypt.util")
const { DateTime } = require("luxon")
const ApiError = require("../utils/errors.util")

const login = async (body) => {
    const oldUser = await Users.findOne({
        include: [{
            model: Roles,
            attributes: ["role_type"],
        }],
        where: {
            email: body.email
        }
    });
    if (!oldUser) {
        throw ApiError.notFound("user not found")
    }

    if (!comparePassword(body.password, oldUser.password)) {
        throw ApiError.notAuthorized("invalid credentials")
    }

    const payload = { id: oldUser.id, firstName: oldUser.firstName, email: oldUser.email, role_type: oldUser.Role.role_type }
    const token = generateToken(payload);
    return token
}

const changePassword = async (userId, reqObj) => {
    const user = await Users.findByPk(userId)
    if (!user) throw ApiError.notFound("user not found")

    if (!comparePassword(reqObj.old_password, user.password)) throw ApiError.notAuthorized("invalid old password")

    const newPw = hashPassword(reqObj.new_password)

    await Users.update({ password: newPw }, { where: { id: userId } })
    return 1
}

const forgotPassword = async (email) => {
    const user = await Users.findOne({ where: { email } })
    if (!user) throw ApiError.notFound("user not found")

    const userInReset = await ResetOtp.findOne({ where: { email } })
    if (userInReset) {
        const otpObj = {
            code: Math.floor(100000 + Math.random() * 900000),
            expiry: DateTime.now().plus({ minutes: 15 }).toISO()
        }

        await ResetOtp.update({ ...otpObj }, { where: { email } })
        return otpObj.code
    } else {
        const otpObj = {
            email,
            code: Math.floor(100000 + Math.random() * 900000),
            expiry: DateTime.now().plus({ minutes: 15 }).toISO()
        }
        await ResetOtp.create(otpObj)
        return otpObj.code
    }
}

const resetPassword = async (email, otp, password) => {
    const otpInDB = await ResetOtp.findOne({ where: { email } })
    if (!otpInDB) throw ApiError.notFound("email not found")

    const otpExpiry = DateTime.fromISO(otpInDB.expiry.toISOString())
    const now = DateTime.now()

    if (now > otpExpiry) throw ApiError.notAuthorized("otp has expired")

    if (otpInDB.code !== parseInt(otp)) throw ApiError.notAuthorized("incorrect otp")

    const newPw = hashPassword(password)

    await Users.update({ password: newPw }, { where: { email } })

    return 1
}

module.exports = {
    login,
    changePassword,
    forgotPassword,
    resetPassword
}