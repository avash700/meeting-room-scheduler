const path = require("path")
const express = require("express");
const cors = require("cors")
const dotenv = require("dotenv");
const bodyParser = require("body-parser")
const schedule = require("node-schedule")
const swaggerUi = require('swagger-ui-express');
// utils and config imports
const db = require("./utils/db.util")
const indexRoutes = require("./routes")
const logger = require("./config/logger.config")
const morgan = require("./config/morgan.config");
const apiErrorHandler = require("./middlewares/errHandler.middleware")
const ApiError = require("./utils/errors.util");
const { notifytUpcomingMeetings } = require("./services/meetings.service")
const swaggerDocument = require('./config/jsdoc.config');
// env config
dotenv.config();

const PORT = process.env.PORT || 8080;

const init = async () => {
    try {
        await db()
        const app = express()

        app.use(cors())
        app.use(bodyParser.urlencoded({ extended: true }))
        app.use(bodyParser.json())
        app.use(morgan.reqHandler)
        // Define routes
        app.use('/api', indexRoutes)
        // not found
        app.get("/", (_, res) => res.status(200).json({ message: "Backend Active!" }))
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
        // catch 404 
        // app.use((_, res) => res.status(404).json({ message: "Route not found!" }));
        app.use((req, res, next) => {
            next(ApiError.notFound("route not found"))
        })

        // error handler
        app.use(apiErrorHandler)
        app.listen(PORT || 8080, () => {
            logger.info(`Server running on port: ${PORT}`)
        })
    } catch (error) {
        logger.error("Something went wrong: ", error)
    }
}

init()
schedule.scheduleJob('*/5 * * * *', function (fireDate) {
    notifytUpcomingMeetings(fireDate)
})
const unexpectedErrorHandler = (error) => {
    logger.error(error);
};

process.on("uncaughtException", unexpectedErrorHandler);
process.on("unhandledRejection", unexpectedErrorHandler);

process.on("SIGTERM", () => {
    logger.info("SIGTERM received");
});


