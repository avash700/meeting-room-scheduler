'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rooms_amenities extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Rooms,{
        foreignKey: 'room_id'
      })

      this.belongsTo(models.Amenities,{
        foreignKey: 'amenity_id'
      })
    }
  }
  rooms_amenities.init({
    room_id: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'rooms'
        },
        key: 'id'
      }
    },
    amenity_id: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'amenities'
        },
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'rooms_amenities',
    modelName: 'RoomsAmenities',
  });
  return rooms_amenities;
};