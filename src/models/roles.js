'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class roles extends Model {
    static associate({ Users }) {
      // define association here
      this.hasMany(Users, {
        foreignKey: "role_id"
      })
    }
  }
  roles.init({
    role_type: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
  }, {
    sequelize,
    modelName: 'Roles',
    tableName: 'roles'
  });
  return roles;
};