'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.Roles, {
        foreignKey: 'role_id'
      })

      this.hasMany(models.Meetings, {
        foreignKey: 'host_id'
      })

      this.belongsToMany(models.Meetings, {
        as: 'invited_meetings',
        through: models.MeetingsUsers,
        foreignKey: 'user_id'
      })
    }
  }
  users.init({
    firstName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    lastName: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    role_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'roles'
        },
        key: 'id'
      }
    }
  }, {
    sequelize,
    modelName: 'Users',
    tableName: 'users'
  });
  return users;
};