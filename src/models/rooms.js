'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Meetings, {
        foreignKey: 'room_id'
      })

      this.belongsToMany(models.Amenities,{
        through: models.RoomsAmenities,
        foreignKey: 'room_id'
      })
    }
  }
  rooms.init({
    room_name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    capacity: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'rooms',
    modelName: 'Rooms'
  });
  return rooms;
};