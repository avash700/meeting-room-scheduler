'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class amenities extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsToMany(models.Rooms,{
        through: models.RoomsAmenities,
        foreignKey: 'amenity_id'
      })
    }
  }
  amenities.init({
    amenity_name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    }
  }, {
    sequelize,
    tableName: 'amenities',
    modelName: 'Amenities',
  });
  return amenities;
};