'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class meetings extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Rooms, {
        foreignKey: 'room_id',
        onDelete: 'RESTRICT',
        onUpdate: 'CASCADE'
      })

      this.belongsTo(models.Users, {
        as: 'host',
        foreignKey: 'host_id',
        onDelete: 'RESTRICT',
        onUpdate: 'CASCADE'
      })
      
      this.belongsToMany(models.Users, {
        as: 'meeting_attendees',
        through: models.MeetingsUsers,
        foreignKey: 'meeting_id'
      })

    }
  }
  meetings.init({
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    description: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    host_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'users'
        },
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    },
    room_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: {
        model: {
          tableName: 'rooms'
        },
        key: 'id'
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT'
    },
    start: {
      allowNull: false,
      type: DataTypes.DATE
    },
    end: {
      allowNull: false,
      type: DataTypes.DATE,

    }
  }, {
    sequelize,
    modelName: 'Meetings',
    tableName: 'meetings',
  });
  return meetings;
};

// deoxygen