const router = require('express').Router();
const roomsController = require('../controllers/rooms.controller');
const { validator, roomsRules, updateRoomsRules, roomsAmenitiesRules, checkRoomAvailabe } = require("../middlewares/validator.middleware")
const { checkAuth } = require("../middlewares/auth.middleware")

/**
    * @openapi
    * /rooms:
    *   get:
    *     tags: [rooms]
    *     summary: returns all rooms
    *     responses:
    *        '200':    
    *            description: success
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [rooms]
    *       summary: add a new room
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            room_name:
    *                                type: string
    *                            capacity:
    *                                type: integer
    *                            amenities:
    *                                type: array
    *                                items:
    *                                   type: integer
    * 
    *       responses:
    *           '201':    
    *               description: room created
    *           '404':
    *               description: amenity doesn't exist
    *           '409':
    *               description: room name already exists
    *           '500':
    *               description: internal server error
    *
    * /rooms/get-by-amenities:
    *    post:
    *       tags: [rooms]
    *       summary: returns room containing selected amenities
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            amenities:
    *                                type: array
    *                                items:
    *                                   type: integer
    *
    *       responses:
    *           '200':    
    *               description: success
    *           '401':
    *               description: unauthorized
    *           '500':
    *               description: internal server error
    * 
    * /rooms/{roomId}:
    *    get:
    *       tags: [rooms]
    *       summary: get a room
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to get 
    *       responses:
    *           '200':    
    *               description: success
    *           '404':
    *               description: room doesn't exist
    *           '500':
    *               description: internal server error
    *    delete:
    *       tags: [rooms]
    *       summary: delete a room
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to delete 
    *       responses:
    *           '200':    
    *               description: room deleted
    *           '400':
    *               description: cannot delete a room booked for meeting
    *           '404':
    *               description: room doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *    put:
    *       tags: [rooms]
    *       summary: update an room
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            room_name:
    *                                type: string
    *                            capacity:
    *                                type: integer
    *                            amenities:
    *                                type: array
    *                                items:
    *                                   type: integer
    *       responses:
    *           '200':    
    *               description: room updated
    *           '404':
    *               description: room doesn't exist
    *           '409':
    *               description: room name already exists
    *           '500':
    *               description: internal server error
    * 
    * /rooms/amenities/{roomId}:
    *    post:
    *       tags: [rooms]
    *       summary: add amenities to an room
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to add amenities
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            amenities:
    *                                type: array
    *                                items:
    *                                   type: integer
    *
    *       responses:
    *           '200':    
    *               description: amenities added to the room
    *           '404':
    *               description: room not found, amenity not found
    *           '409':
    *               description: amenity already exists in the room
    *           '500':
    *               description: internal server error
    *    delete:
    *       tags: [rooms]
    *       summary: remove amenity from an room
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to remove amenities
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            amenities:
    *                                type: array
    *                                items:
    *                                   type: integer
    *
    *       responses:
    *           '200':    
    *               description: amenities removed successfully
    *           '404':
    *               description: room doesn't exist or amenity doesn't exist
    *           '409':
    *               description: amenity does not exist in the room
    *           '500':
    *               description: internal server error
    * 
    * 
    * /rooms/check-availability/{roomId}:
    *    post:
    *       tags: [rooms]
    *       summary: check availability of room in the given time slot
    *       parameters:
    *         - name: roomId
    *           in: path
    *           required: true
    *           description: the id of the room to check availability
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                          start:
    *                              type: string
    *                              example: 2022-04-12T11:45:00
    *                          end:
    *                              type: string
    *                              example: 2022-04-12T12:00:00
    *                            
    *
    *       responses:
    *           '200':    
    *               description: room status
    *           '404':
    *               description: room not found, amenity not found
    *           '500':
    *               description: internal server error
    */


router.route('/')
    .get(checkAuth(["admin", "employee"]), roomsController.index)
    .post(checkAuth("admin"), roomsRules(), validator, roomsController.create)

router.route('/get-by-amenities')
    .post(checkAuth("admin","employee"),roomsAmenitiesRules(),validator, roomsController.getByAmenities)

router.route('/:id')
    .get(checkAuth(["admin", "employee"]), roomsController.getOne)
    .delete(checkAuth("admin"), roomsController.destroy)
    .put(checkAuth("admin"), updateRoomsRules(), validator, roomsController.update)

router.route('/amenities/:id')
    .post(checkAuth("admin"), roomsAmenitiesRules(), validator, roomsController.addAmenities)
    .delete(checkAuth("admin"), roomsAmenitiesRules(), validator, roomsController.removeAmenities)

router.route('/check-availability/:id')
    .post(checkAuth(["admin", "employee"]), checkRoomAvailabe(), validator, roomsController.checkAvailability)

module.exports = router

