const router = require('express').Router();
const amenitiesController = require('../controllers/amenities.controller');
const { validator, amenitiesRules } = require("../middlewares/validator.middleware")
const { checkAuth } = require("../middlewares/auth.middleware")

/**
    * @openapi
    * /amenities:
    *   get:
    *     tags: [amenities]
    *     summary: returns all amenities
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: unauthorized
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [amenities]
    *       summary: add a new amenity
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            amenity_name:
    *                                type: string
    *       responses:
    *           '201':    
    *               description: successfully created an amenity
    *           '401':
    *               description: not authorized
    *           '409':
    *               description: amenity already exists
    *           '500':
    *               description: internal server error
    * 
    * 
    * /amenities/{amenityId}:
    *    get:
    *       tags: [amenities]
    *       summary: get an amenity
    *       parameters:
    *         - name: amenityId
    *           in: path
    *           required: true
    *           description: the id of the amenity to get 
    *       responses:
    *           '200':    
    *               description: success
    *           '400':
    *               description: can't delete an amenity in use
    *           '401':
    *               description: not authorized
    *           '404':
    *               description: amenity not found
    *           '500':
    *               description: internal server error
    * 
    *    delete:
    *       tags: [amenities]
    *       summary: delete a amenity
    *       parameters:
    *         - name: amenityId
    *           in: path
    *           required: true
    *           description: the id of the amenity to delete 
    *       responses:
    *           '200':    
    *               description: amenity deleted successfully
    *           '404':
    *               description: amenity not found
    *           '401':
    *               description: unauthorized
    *           '500':
    *               description: internal server error
    * 
    *    put:
    *       tags: [amenities]
    *       summary: update a amenity
    *       parameters:
    *         - name: amenityId
    *           in: path
    *           required: true
    *           description: the id of the amenity to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            amenity_name:
    *                                type: string 
    *       responses:
    *           '200':    
    *               description: amenity updated
    *           '404':
    *               description: amenity not found
    *           '401':
    *               description: unauthorized
    *           '409':
    *               description: amenity already exists
    *           '500':
    *               description: internal server error
    *          
    */


router.route('/')
    .get(checkAuth(["admin", "employee"]), amenitiesController.index)
    .post(checkAuth("admin"), amenitiesRules(), validator, amenitiesController.create)

router.route('/:id')
    .get(checkAuth(["admin", "employee"]), amenitiesController.getOne)
    .delete(checkAuth("admin"), amenitiesController.destroy)
    .put(checkAuth("admin"), amenitiesRules(), validator, amenitiesController.update)

module.exports = router

