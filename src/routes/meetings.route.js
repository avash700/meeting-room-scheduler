const router = require('express').Router();
const meetingsController = require('../controllers/meetings.controller');
const { validator, meetingRules, userMeetingRules, updateMeetingRules } = require("../middlewares/validator.middleware")

/**
    * @openapi
    * /meetings:
    *   get:
    *     tags: [meetings]
    *     parameters:
    *       - name: room
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the room to filter by
    *       - name: date
    *         in: query
    *         required: false
    *         schema:
    *           type: string
    *           example: 2022,2022/2,2022/2/1
    *         description: the date of the room to filter by
    *       - name: limit
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: number of results per page, default is 10 per page
    *       - name: page
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 2
    *         description: page number of results, default is page 1
    *     summary: returns all meetings
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [meetings]
    *       summary: create a new meeting
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            room_id:
    *                                type: integer
    *                            name:
    *                                type: string
    *                            description:
    *                                type: string
    *                            start:
    *                                type: string
    *                                example: 2022-04-12T11:45:00
    *                            end:
    *                                type: string
    *                                example: 2022-04-12T12:00:00
    *                            attendees:
    *                                type: array
    *                                items:
    *                                   type: integer
    *       responses:
    *           '201':    
    *               description: meeting created
    *           '400':
    *               description: room not available in the time slot, start time must be greater than end time, start time must be 30 minutes ahead a current time
    *           '401':
    *               description: not authorized
    *           '404':
    *               description: user doesnot exist
    *           '500':
    *               description: internal server error
    * /meetings/hosted-meetings:
    *   get:
    *     tags: [meetings]
    *     parameters:
    *       - name: room
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the room to filter by
    *       - name: date
    *         in: query
    *         required: false
    *         schema:
    *           type: string
    *           example: 2022,2022/2,2022/2/1
    *         description: the date of the room to filter by
    *       - name: limit
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: number of results per page, default is 10 per page
    *       - name: page
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 2
    *         description: page number of results, default is page 1
    *     summary: returns all upcoming hosted meetings from current date time if no parameters applied 
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '500':
    *            description: internal server error
    * /meetings/invited-meetings:
    *   get:
    *     tags: [meetings]
    *     parameters:
    *       - name: room
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the room to filter by
    *       - name: date
    *         in: query
    *         required: false
    *         schema:
    *           type: string
    *           example: 2022,2022/2,2022/2/1
    *         description: the date of the room to filter by
    *       - name: limit
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: number of results per page, default is 10 per page
    *       - name: page
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 2
    *         description: page number of results, default is page 1
    *     summary: returns all invited meetings to attend from current date time if no parameters applied 
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '500':
    *            description: internal server error
    * /meetings/upcoming-meetings:
    *   get:
    *     tags: [meetings]
    *     parameters:
    *       - name: room
    *         in: query
    *         required: false
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the room to filter by
    *     summary: returns all upcoming meetings to attend from current time,hosted and invited
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '500':
    *            description: internal server error
    * /meetings/{id}:
    *   get:
    *     tags: [meetings]
    *     parameters:
    *       - name: id
    *         in: path
    *         required: true
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the meeting to get
    *     summary: get a meeting
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '404':
    *            description: meeting not found
    *        '500':
    *            description: internal server error
    *   delete:
    *     tags: [meetings]
    *     parameters:
    *       - name: id
    *         in: path
    *         required: true
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the meeting to delete
    *     summary: delete a meeting
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: not authorized
    *        '404':
    *            description: meeting not found
    *        '500':
    *            description: internal server error
    * 
    *   put:
    *     tags: [meetings]
    *     parameters:
    *       - name: id
    *         in: path
    *         required: true
    *         schema:
    *           type: integer
    *           example: 5
    *         description: the id of the meeting to update
    *     summary: update a meeting
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          room_id:
    *                              type: integer
    *                          name:
    *                              type: string
    *                          description:
    *                              type: string
    *                          start:
    *                              type: string
    *                              example: 2022-04-12T11:45:00
    *                          end:
    *                              type: string
    *                              example: 2022-04-12T12:00:00
    *                          attendees:
    *                              type: array
    *                              items:
    *                               type: integer
    *     responses:
    *         '201':    
    *             description: meeting created
    *         '400':
    *             description: room not available in the time slot, start time must be greater than end time, start time must be 30 minutes ahead a current time
    *         '401':
    *             description: not authorized
    *         '500':
    *             description: internal server error
    * /meetings/attendees/{id}:
    *   post:
    *       parameters:
    *         - name: id
    *           in: path
    *           required: true
    *           description: id of the meeting to add new attendees
    *       tags: [meetings]
    *       summary: add attendees to a meeting
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                       type: object
    *                       properties:
    *                           attendees:
    *                               type: array
    *                               items:
    *                                   type: integer
    *       responses:
    *           '200':    
    *               description: attendees added
    *           '400':
    *               description: attendees already added
    *           '401':
    *               description: not authorized
    *           '404':
    *               description: meeting not found, user does not exist
    *           '500':
    *               description: internal server error
    * 
    *   delete:
    *       parameters:
    *         - name: id
    *           in: path
    *           required: true
    *           description: id of the meeting to remove attendees
    *       tags: [meetings]
    *       summary: remove attendees from a meeting
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                       type: object
    *                       properties:
    *                           attendees:
    *                               type: array
    *                               items:
    *                                   type: integer
    *       responses:
    *           '200':    
    *               description: attendees removed
    *           '400':
    *               description: attendees already added
    *           '401':
    *               description: not authorized
    *           '404':
    *               description: meeting not found, user does not exist
    *           '500':
    *               description: internal server error
    */

router.route('/')
    .get(meetingsController.index)
    .post(meetingRules(), validator, meetingsController.create)

router.route('/hosted-meetings')
    .get(meetingsController.getHostedMeetings)

router.route('/invited-meetings')
    .get(meetingsController.getInvitedMeetings)

router.route('/upcoming-meetings')
    .get(meetingsController.getAllUpcomingMeetings)

router.route('/:id')
    .get(meetingsController.getOne)
    .delete(meetingsController.destroy)
    .put(updateMeetingRules(), validator, meetingsController.update)


router.route('/attendees/:id')
    .post(userMeetingRules(), validator, meetingsController.addUsers)
    .delete(userMeetingRules(), validator, meetingsController.removeUsers)

module.exports = router

