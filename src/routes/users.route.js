const router = require('express').Router();
const usersController = require('../controllers/users.controller');
const { userRules, userUpdateRules,validator } = require("../middlewares/validator.middleware")
const { checkAuth } = require("../middlewares/auth.middleware")


/**
    * @openapi
    * /users:
    *   get:
    *     tags: [user]
    *     summary: returns all users
    *     responses:
    *        '200':    
    *            description: success
    *        '500':
    *            description: internal server error
    *   post:
    *     tags: [user]
    *     summary: add a new user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          role_id:
    *                              type: integer
    *                          firstName:
    *                              type: string
    *                          lastName:
    *                              type: string
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: user created
    *         '404':
    *             description: role not found
    *         '409':
    *             description: user email already exists
    *         '500':
    *             description: internal server error
    *   put:
    *       tags: [user]
    *       summary: update a user
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            firstName:
    *                                type: string
    *                            lastName:
    *                                type: string
    *                            email:
    *                                type: string
    *       responses:
    *           '200':    
    *               description: user updated
    *           '404':
    *               description: user doesn't exist
    *           '409':
    *               description: email already exists
    *           '500':
    *               description: internal server error
    * 
    * 
    * /users/{userId}:
    *   get:
    *       tags: [user]
    *       summary: get a user
    *       parameters:
    *         - name: userId
    *           in: path
    *           required: true
    *           description: the id of the user to get
    *       responses:
    *           '200':    
    *               description: user deleted
    *           '404':
    *               description: user doesn't exist
    *           '500':
    *               description: internal server error
    *   delete:
    *       tags: [user]
    *       summary: delete a user
    *       parameters:
    *         - name: userId
    *           in: path
    *           required: true
    *           description: the id of the user to delete 
    *       responses:
    *           '200':    
    *               description: user deleted
    *           '400':
    *               description: cannot delete user who is hosting or attending a meeting
    *           '404':
    *               description: user doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *          
    */


router.route('/')
    .get(checkAuth(["admin","employee"]),usersController.index)
    .post(checkAuth("admin"),userRules(), validator, usersController.create)
    .put(checkAuth(["employee","admin"]),userUpdateRules(), validator,usersController.update)

router.route('/:id')
    .get(checkAuth(["admin","employee"]),usersController.getOne)
    .delete(checkAuth("admin"),usersController.destroy)



module.exports = router



