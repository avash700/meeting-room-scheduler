const router = require('express').Router();
const authController = require('../controllers/auth.controller');
const { checkAuth } = require('../middlewares/auth.middleware')
const { loginRules, validator, changePasswordRules, forgotPasswordRules, resetPasswordRules } = require("../middlewares/validator.middleware")

/**
    * @openapi
    * /auth/login:
    *   post:
    *     tags: [auth]
    *     summary: login a user
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: login success
    *         '404':
    *             description: user does not exist
    *         '401':
    *             description: user not found
    *         '500':
    *             description: internal server error
    * /auth/change-password:
    *   post:
    *     tags: [auth]
    *     summary: change user password
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          old_password:
    *                              type: string
    *                          new_password:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: password updated successfully
    *         '400':
    *             description: password criteria doesn't match
    *         '404':
    *             description: user does not exist
    *         '401':
    *             description: not authorized, invalid old password
    *         '500':
    *             description: internal server error
    * 
    * /auth/forgot-password:
    *   post:
    *     tags: [auth]
    *     summary: request to reset password
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *     responses:
    *         '200':    
    *             description: success, otp sent in email
    *         '404':
    *             description: user not found
    *         '500':
    *             description: internal server error
    * /auth/reset-password:
    *   post:
    *     tags: [auth]
    *     summary: reset password with the provided otp
    *     requestBody:
    *         required: true
    *         content:
    *             application/json:
    *                  schema:
    *                      type: object
    *                      properties:
    *                          email:
    *                              type: string
    *                          password:
    *                              type: string
    *                          otp:
    *                              type: integer
    *     responses:
    *         '200':    
    *             description: password reset successful
    *         '404':
    *             description: email not found
    *         '401':
    *             description: incorrect otp, otp expired
    *         '500':
    *             description: internal server error
    */


router.route('/login')
    .post(loginRules(), validator, authController.loginUser)

router.route('/change-password')
    .post(checkAuth(["admin", "employee"]), changePasswordRules(), validator, authController.changePassword)

router.route('/forgot-password')
    .post(forgotPasswordRules(), validator, authController.forgotPassword)

router.route('/reset-password')
    .post(resetPasswordRules(),validator,authController.resetPassword)

module.exports = router



