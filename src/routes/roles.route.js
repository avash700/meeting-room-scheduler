const router = require('express').Router();
const rolesController = require('../controllers/roles.controller');
const { validator, rolesRules } = require("../middlewares/validator.middleware")

/**
    * @openapi
    * /roles:
    *   get:
    *     tags: [role]
    *     summary: returns all roles
    *     responses:
    *        '200':    
    *            description: success
    *        '401':
    *            description: Unauthorized               
    *        '500':
    *            description: internal server error
    *   post:
    *       tags: [role]
    *       summary: add a new role
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            role_type:
    *                                type: string
    *                         
    *       responses:
    *           '200':    
    *               description: role created successfully
    *           '409':
    *               description: role already exists
    *           '500':
    *               description: internal server error
    * 
    * 
    * /roles/{roleId}:
    *    get:
    *       tags: [role]
    *       summary: get one role
    *       parameters:
    *         - name: roleId
    *           in: path
    *           required: true
    *           description: the id of the role to get 
    *       responses:
    *           '200':    
    *               description: success
    *           '404':
    *               description: role doesn't exist
    *           '500':
    *               description: internal server error    
    *    delete:
    *       tags: [role]
    *       summary: delete a role
    *       parameters:
    *         - name: roleId
    *           in: path
    *           required: true
    *           description: the id of the role to delete 
    *       responses:
    *           '200':    
    *               description: role deleted
    *           '400':
    *               description: cannot delete a role in use
    *           '404':
    *               description: role doesn't exist
    *           '500':
    *               description: internal server error
    * 
    *    put:
    *       tags: [role]
    *       summary: update a role
    *       parameters:
    *         - name: roleId
    *           in: path
    *           required: true
    *           description: the id of the role to update
    *       requestBody:
    *           required: true
    *           content:
    *               application/json:
    *                    schema:
    *                        type: object
    *                        properties:
    *                            role_type:
    *                                type: string
    *                            
    *       responses:
    *           '200':    
    *               description: role updated
    *           '400':
    *               description: cannot update a role in use
    *           '404':
    *               description: role doesn't exist
    *           '409':
    *               description: role already exists
    *           '500':
    *               description: internal server error
    *          
    */

router.route('/')
    .get(rolesController.index)
    .post(rolesRules(), validator, rolesController.create)

router.route('/:id')
    .get(rolesController.getOne)
    .delete(rolesController.destroy)
    .put(rolesRules(), validator, rolesController.update)

module.exports = router

