const router = require("express").Router();
const rolesRoute = require("./roles.route")
const usersRoute = require("./users.route")
const authRoute = require("./auth.route")
const roomsRoute = require("./rooms.route");
const meetingsRoute = require("./meetings.route")
const amenitiesRoute = require("./amenities.route")

// check auth
const { checkAuth } = require("../middlewares/auth.middleware")


router.use('/auth', authRoute)
router.use('/roles', checkAuth("admin") ,rolesRoute)
router.use('/users', usersRoute)
router.use('/rooms', roomsRoute)
router.use('/meetings', checkAuth(["admin","employee"]), meetingsRoute)
router.use('/amenities',amenitiesRoute)

module.exports = router;