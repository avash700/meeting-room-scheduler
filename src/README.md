# MEETING ROOM SCHEDULER

### install dependencies
```
yarn install
```

### create database
```
yarn sequelize-cli db:create
```

### run migratins
```
yarn sequelize-cli db:migrate
```

### run seeds
```
yarn sequelize-cli db:seed:all
```

### start the server
```
yarn dev
```