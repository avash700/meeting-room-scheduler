const roomsService = require("../services/rooms.service")
const { pick, removeUndefinedKeys } = require("../utils/object.util")

// get all rooms
const index = async (req, res, next) => {
    try {
        const rooms = await roomsService.getRooms()
        return res.status(200).json({ message: "success", data: rooms });
    } catch (e) {
        next(e)
    }
}

// get one room
const getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const room = await roomsService.getRoom(id)
        return res.status(200).json({ message: "success", data: room })
    } catch (e) {
        next(e)
    }
}

// create a new room
const create = async (req, res, next) => {
    try {
        const reqObj = pick(req.body, ['room_name','capacity','amenities']);
        await roomsService.createRoom(reqObj)
        return res.status(201).json({ message: "successfully created a room" })
    } catch (e) {
        next(e)
    }
}

// update room
const update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const reqObj = removeUndefinedKeys(pick(req.body,["room_name",'capacity','amenities']))
        await roomsService.updateRoom(id,reqObj)
        return res.status(200).json({ message: "room updated successfully" });
    } catch (e) {
        next(e)
    }
}

// delete an existing rooms
const destroy = async (req, res, next) => {
    try {
        const { id } = req.params;
        await roomsService.deleteRoom(id)
        return res.status(200).json({ message: "room deleted successfully" })
    } catch (e) {
        next(e)
    }
}

const addAmenities = async (req, res, next) => {
    try {
        const { id } = req.params
        const amenitiesToAdd = pick(req.body, ["amenities"])
        await roomsService.addAmenitiesToRoom(id,amenitiesToAdd.amenities)
        return res.status(200).json({ message: "amenities added successfully" })
    } catch (e) {
        next(e)
    }
}

const removeAmenities = async (req, res, next) => {
    try {
        const { id } = req.params
        const amenitiesToRemove = pick(req.body, ["amenities"])
        await roomsService.removeAmenitiesFromRoom(id,amenitiesToRemove.amenities)
        return res.status(200).json({ message: "amenities removed successfully" })
    } catch (e) {
        next(e)
    }
}

const checkAvailability = async(req,res,next) => {
    try{
        const {id} = req.params
        const reqObj = pick(req.body,["start","end"])
        const isAvailable = await roomsService.checkAvailability(id,reqObj)
        return res.status(200).json(isAvailable)
    }catch(e){
        next(e)
    }
}

const getByAmenities = async (req,res,next) => {
    try{
        const options = pick(req.body, ["amenities"])
        const rooms = await roomsService.getRoomsByAmenities(options)
        return res.status(200).json({ message: "success", data: rooms });
    }catch(e){
        next(e)
    }
}

module.exports = {
    index,
    getOne,
    create,
    update,
    destroy,
    addAmenities,
    removeAmenities,
    checkAvailability,
    getByAmenities
}