const amenitiesService = require("../services/amenities.service")
const { pick, removeUndefinedKeys } = require("../utils/object.util")

// get all amenities
const index = async (_, res, next) => {
    try {
        const amenities = await amenitiesService.getAmenities()
        return res.status(200).json({ message: "success", data: amenities });
    } catch (e) {
        next(e)
    }
}

// get one amenity
const getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const amenity = await amenitiesService.getAmenity(id)
        return res.status(200).json({ message: "success", data: amenity })
    } catch (e) {
        next(e)
    }
}

// create a new amenity
const create = async (req, res, next) => {
    try {
        const reqObj = pick(req.body, ['amenity_name']);
        await amenitiesService.createAminity(reqObj)
        return res.status(201).json({ message: "successfully created an amenity" })
    } catch (e) {
        next(e)
    }
}

// update amenity
const update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const reqObj = removeUndefinedKeys(pick(req.body,["amenity_name"]))
        await amenitiesService.updateAmenity(id,reqObj)
        return res.status(200).json({ message: "amenity updated successfully" });
    } catch (e) {
        next(e)
    }
}

// delete an existing amenities
const destroy = async (req, res, next) => {
    try {
        const { id } = req.params;
        await amenitiesService.deleteAmenity(id)
        return res.status(200).json({ message: "amenity deleted successfully" })
    } catch (e) {
        next(e)
    }
}


module.exports = {
    index,
    getOne,
    create,
    update,
    destroy
}