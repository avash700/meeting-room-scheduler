const rolesService = require('../services/roles.service');
const logger = require("../config/logger.config")
const { pick, removeUndefinedKeys } = require("../utils/object.util")

// get all roles
const index = async (_, res, next) => {
    try {
        const roles = await rolesService.getRoles()
        return res.status(200).json({ message: "success", data: roles });
    } catch (e) {
        next(e)
    }
}

// get one role
const getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const role = await rolesService.getRole(id)
        return res.status(200).json({ message: "success", data: role })
    } catch (e) {
        next(e)
    }
}

// create a new role
const create = async (req, res, next) => {
    try {
        const reqObj = pick(req.body, ['role_type']);
        await rolesService.createRole(reqObj)
        return res.status(201).json({ message: "successfully created a new role" })
    } catch (e) {
        next(e)
    }
}

// update role
const update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const reqObj = removeUndefinedKeys(pick(req.body, ["role_type"]))
        await rolesService.updateRole(id, reqObj)
        return res.status(200).json({ message: "role updated successfully" });
    } catch (e) {
        next(e)
    }
}

// delete an existing roles
const destroy = async (req, res, next) => {
    try {
        const { id } = req.params;
        await rolesService.deleteRole(id)
        return res.status(200).json({ message: "role deleted successfully" })
    } catch (e) {
        next(e)
    }
}


module.exports = {
    index,
    getOne,
    create,
    update,
    destroy
}