const authService = require("../services/auth.service")
const { pick } = require("../utils/object.util")
const { sendEmail } = require("../utils/mailer.util")

const loginUser = async (req, res, next) => {
    try {
        const body = pick(req.body, ['email', 'password']);
        const token = await authService.login(body);
        return res.status(200).send({ message: "login success", token })
    } catch (e) {
        next(e)
    }
}

const changePassword = async (req, res, next) => {
    try {
        const { id: userId } = req.user
        const reqObj = pick(req.body, ["old_password", "new_password"])
        await authService.changePassword(userId, reqObj)
        return res.status(200).json({ message: "password updated successfully" })
    } catch (e) {
        next(e)
    }
}

const forgotPassword = async (req, res, next) => {
    try {
        const { email } = pick(req.body, ["email"])
        const otp = await authService.forgotPassword(email)

        res.status(200).json({ message: "otp has been sent to your email. expires in 15 minutes" })

        await sendEmail(email, "password reset otp", `Here is your otp to reset password: ${otp}`)
        return 1
    } catch (e) {
        next(e)
    }
}

const resetPassword = async (req, res, next) => {
    try {
        const { email, otp, password } = pick(req.body, ["email", "otp", "password"])
        await authService.resetPassword(email, otp, password)
        return res.status(200).json({ message: "password reset successful" })
    } catch (e) {
        next(e)
    }
}


module.exports = {
    loginUser,
    changePassword,
    forgotPassword,
    resetPassword
}