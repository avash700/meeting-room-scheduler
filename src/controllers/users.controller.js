const usersService = require("../services/users.service");
const { transporter, newAccount } = require("../utils/mailer.util");
const { pick, removeUndefinedKeys } = require("../utils/object.util");

// get users
const index = async (_, res, next) => {
    try {
        const users = await usersService.getUsers()
        return res.status(200).json({ message: "Success", data: users });
    } catch (e) {
        next(e)
    }
}

// get one role
const getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const user = await usersService.getUser(id)
        return res.status(200).json({ message: "success", data: user })
    } catch (e) {
        next(e)
    }
}

// create a new user
const create = async (req, res, next) => {
    try {
        const reqObj = pick(req.body, ['firstName', 'lastName', 'email', 'password', 'role_id']);
        const userDetails = await usersService.createUser(reqObj)
        res.status(201).json({ message: "User created successfully." })
        await transporter.sendMail(newAccount("account created for gurzu scheduler", userDetails))
    } catch (e) {
        next(e)
    }
}

// update a user
const update = async (req, res, next) => {
    try {
        const { id: userId } = req.user;
        const reqObj = removeUndefinedKeys(pick(req.body, ['firstName', 'lastName', 'email']));
        await usersService.updateUser(userId, reqObj)
        return res.status(200).json({ message: "User updated successfully." })
    } catch (e) {
        next(e)
    }
}

// delete an existing user
const destroy = async (req, res, next) => {
    try {
        const { id } = req.params;
        await usersService.deleteUser(id)
        return res.status(200).json({ message: "user deleted successfully" })
    } catch (e) {
        next(e)
    }
}

module.exports = {
    index,
    getOne,
    create,
    update,
    destroy
}