const meetingsService = require("../services/meetings.service")
const { pick, removeUndefinedKeys } = require("../utils/object.util")
const { transporter, meetingOption, newInvitationOption, withdrawalOption } = require("../utils/mailer.util")

// get all meetings
const index = async (req, res, next) => {
    try {
        const options = req.query
        const meetings = await meetingsService.getMeetings(options)
        return res.status(200).json({ message: "success", data: meetings });
    } catch (e) {
        next(e)
    }
}

// get one meeting
const getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const meeting = await meetingsService.getMeeting(id)
        return res.status(200).json({ message: "success", data: meeting })
    } catch (e) {
        next(e)
    }
}

// create a new meeting
const create = async (req, res, next) => {
    try {
        const { id: userId } = req.user
        const reqObj = pick(req.body, ["room_id", "name", "description", "start", "end", "attendees"]);
        reqObj.host_id = userId
        const meetingData = await meetingsService.createMeeting(reqObj)
        res.status(201).json({ message: "successfully created a new meeting" })
        await transporter.sendMail(meetingOption("call to join a scheduled meeting", meetingData))
        return 1
    } catch (e) {
        next(e)
    }
}

// update meeting
const update = async (req, res, next) => {
    try {
        const { id } = req.params;
        const { id: userId } = req.user;
        const reqObj = removeUndefinedKeys(pick(req.body, ["room_id", "name", "description", "start", "end", "attendees"]))
        const meetingData = await meetingsService.updateMeeting(id, userId, reqObj)
        res.status(200).json({ message: "meeting updated successfully" });
        if (meetingData.addedMail.length > 0) await transporter.sendMail(newInvitationOption("you are invited to a meeting", meetingData))
        await transporter.sendMail(meetingOption("information about updated meeting details", meetingData))
        if (meetingData.removedMail.length > 0) await transporter.sendMail(withdrawalOption("invitation revoked from meeting", meetingData))
        return 1
    } catch (e) {
        next(e)
    }
}

// delete an existing rooms
const destroy = async (req, res, next) => {
    try {
        const { id } = req.params
        const { id: userId } = req.user;
        const deletedMeeting = await meetingsService.deleteMeeting(id, userId)
        res.status(200).json({ message: "meeting deleted successfully" })
        await transporter.sendMail(meetingOption("information about cancellation of meeting", deletedMeeting))
        return 1
    } catch (e) {
        next(e)
    }
}

const addUsers = async (req, res, next) => {
    try {
        const { id } = req.params
        const { id: userId } = req.user
        const usersToAdd = pick(req.body, ["attendees"])
        const meetingData = await meetingsService.addUsersToMeeting(id, userId, usersToAdd.attendees)
        res.status(200).json({ message: "users added successfully" })
        await transporter.sendMail(meetingOption("invitation to meeting", meetingData))
        return 1
    } catch (e) {
        next(e)
    }
}

const removeUsers = async (req, res, next) => {
    try {
        const { id } = req.params
        const { id: userId } = req.user
        const usersToRemove = pick(req.body, ["attendees"])
        const meetingData = await meetingsService.removeUsersFromMeeting(id, userId, usersToRemove.attendees)
        res.status(200).json({ message: "users removed successfully" })
        await transporter.sendMail(meetingOption("invitation withdrawn from meeting", meetingData))
        return 1
    } catch (e) {
        next(e)
    }
}

const getInvitedMeetings = async (req, res, next) => {
    try {
        const options = req.query
        const { id: userId } = req.user
        const meetings = await meetingsService.getInvitedUserMeetings(options, userId)
        return res.status(200).json({ message: "success", data: meetings })
    } catch (e) {
        next(e)
    }
}

const getHostedMeetings = async (req, res, next) => {
    try {
        const options = req.query
        const { id: userId } = req.user
        const meetings = await meetingsService.getHostedUserMeetings(options, userId)
        return res.status(200).json({ message: "success", data: meetings })
    } catch (e) {
        next(e)
    }
}

const getAllUpcomingMeetings = async (req, res, next) => {
    try {
        const options = req.query
        const { id: userId } = req.user
        const meetings = await meetingsService.getTodaysUpcomingMeetings(options, userId)
        return res.status(200).json({ message: "success", data: meetings })
    } catch (e) {
        next(e)
    }
}


module.exports = {
    index,
    getOne,
    create,
    update,
    destroy,
    addUsers,
    removeUsers,
    getInvitedMeetings,
    getHostedMeetings,
    getAllUpcomingMeetings
}