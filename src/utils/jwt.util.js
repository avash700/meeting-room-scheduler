const jwt = require('jsonwebtoken');

const secret = "yC1fYtLtTC";

const generateToken = (payload) => {
    return jwt.sign(payload, secret, { expiresIn: '6h' });
}

const validateToken = async (token) => {
    try {
        return { data: jwt.verify(token, secret) };
    } catch (e) {
        return { error: "Invalid Token" };
    }
}

module.exports = {
    generateToken, validateToken
}