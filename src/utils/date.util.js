const { DateTime } = require("luxon")

const isOverLap = (t1Start,t1End,t2Start,t2End) => {
    const startA = DateTime.fromISO(t1Start)
    const endA = DateTime.fromISO(t1End)

    const startB = DateTime.fromISO(t2Start)
    const endB = DateTime.fromISO(t2End)
    // check if time overlaps preciesly
    return (startA <= endB) && (endA >= startB)
    
    // to allow t1End=2022-05-05T12:00:00 and t2Start=2022-05-05T12:00:00
    // return (startA < endB) && (endA > startB)
}

// minutesGap is the gap between two time intervals in minutes that is required
const isOverLapWithGap = (t1Start,t1End,t2Start,t2End,minutesGap) => {
    const startA = DateTime.fromISO(t1Start)
    const endA = DateTime.fromISO(t1End)

    const startB = DateTime.fromISO(t2Start).minus({minutes: minutesGap})
    const endB = DateTime.fromISO(t2End).plus({minutes: minutesGap})

    // check if time overlaps preciesly
    return (startA < endB) && (endA > startB)
}

// console.log(isOverLap("2022-05-05T12:00:00","2022-05-05T13:00:00","2022-05-05T13:01:00","2022-05-05T14:00:00"))
// console.log(isOverLapWithGap("2022-05-05T12:00:00","2022-05-05T13:00:00","2022-05-05T13:05:00","2022-05-05T14:00:00",5))
module.exports = {
    isOverLap,
    isOverLapWithGap
}