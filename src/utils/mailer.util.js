const nodemailer = require("nodemailer")

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // true for 465
    auth: {
        user: 'gurzu.scheduler@gmail.com',
        pass: 'gurzu123',
    },
});

const sendEmail = async (to, subject, text) => {
    await transporter.sendMail({
        from: 'gurzu.scheduler@gmail.com',
        to,
        subject,
        text
    });
}


const meetingOption = (subject, data) => {
    return {
        from: "gurzu.scheduler@gmail.com",
        to: [...data.attendeeMail],
        subject,
        html: `<body style="margin:0;padding:0;">
        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                                    <tr>
                                        <td style="padding:0 0 36px 0;color:#153643;">
                                            <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">meeting details:</h1>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">name: ${data.name}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">description: ${data.description}</p>
                     
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting start time: ${data.meetingStart}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting end date: ${data.meetingEnd}</p>
                                            </br>
                        </br>
      
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">at room: ${data.room}</p>
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">meeting hosted by: ${data.hostFirstName}, ${data.hostLastName}</p>
                                        </td>
                                    </tr>
        </table>
    </body>`
    }
}

const newInvitationOption = (subject, data) => {
    return {
        from: "gurzu.scheduler@gmail.com",
        to: [...data.addedMail],
        subject,
        html: `<body style="margin:0;padding:0;">
        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                                    <tr>
                                        <td style="padding:0 0 36px 0;color:#153643;">
                                            <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">meeting details:</h1>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">name: ${data.name}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">description: ${data.description}</p>
                     
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting start time: ${data.meetingStart}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting end date: ${data.meetingEnd}</p>
                                            </br>
                        </br>
      
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">at room: ${data.room}</p>
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">meeting hosted by: ${data.hostFirstName}, ${data.hostLastName}</p>
                                        </td>
                                    </tr>
        </table>
    </body>`
    }
}

const withdrawalOption = (subject, data) => {
    return {
        from: "gurzu.scheduler@gmail.com",
        to: [...data.removedMail],
        subject,
        html: `<body style="margin:0;padding:0;">
        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                                    <tr>
                                        <td style="padding:0 0 36px 0;color:#153643;">
                                            <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">meeting details:</h1>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">name: ${data.name}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">description: ${data.description}</p>
                     
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting start time: ${data.meetingStart}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Meeting end date: ${data.meetingEnd}</p>
                                            </br>
                        </br>
      
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">at room: ${data.room}</p>
                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">meeting hosted by: ${data.hostFirstName}, ${data.hostLastName}</p>
                                        </td>
                                    </tr>
        </table>
    </body>`
    }
}

const newAccount = (subject, data) => {
    return {
        from: "gurzu.scheduler@gmail.com",
        to: data.email,
        subject,
        html: `<body style="margin:0;padding:0;">
        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
                                    <tr>
                                        <td style="padding:0 0 36px 0;color:#153643;">
                                            <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">Account created for gurzu scheduler. Please login using the following credentials and change your password.</h1>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">email: ${data.email}</p>
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">password: ${data.password}</p>
                     
                                            <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;">Role: ${data.role}</p>
                                            </br>
                                        </td>
                                    </tr>
        </table>
    </body>`
    }
}


module.exports = { transporter, meetingOption, newInvitationOption, withdrawalOption, newAccount, sendEmail }