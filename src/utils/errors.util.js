class ApiError {
    constructor(code,message){
        this.code = code;
        this.message = message;
    }

    static badRequest(msg){
        return new ApiError(400,msg)
    }

    static notFound(msg){
        return new ApiError(404,msg)
    }

    static alreadyExists(msg){
        return new ApiError(409,msg)
    }

    static internalError(msg){
        return new ApiError(500,msg)
    }

    static notAuthorized(msg){
        return new ApiError(401,msg)
    }
}

module.exports = ApiError