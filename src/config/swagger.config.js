module.exports = {
  "openapi": "3.0.0",
  "info": {
    "title": "meeting room scheduler",
    "version": "1.0"
  },
  "servers": [
    {
      "url": "http://localhost:{port}/api",
      "variables": {
        "port": {
          "default": 8080,
          "description": "base api url"
        }
      }
    }
  ],
  "components": {
    "securitySchemes": {
      "jwtAuth": {
        "type": "http",
        "scheme": "bearer"
      }
    }
  },
  "security": [
    {
      "jwtAuth": []
    }
  ],
  "tags": [
    {
      "name": "auth",
      "description": "authentication and authorization of users"
    },
    {
      "name": "amenities",
      "description": "amenitties a room can have"
    },
    {
      "name": "rooms",
      "description": "the rooms that can be booked for a meeting"
    },
    {
      "name": "meetings",
      "description": "the scheduled meetings"
    },
    {
      "name": "user",
      "description": "available users"
    },
    {
      "name": "role",
      "description": "available roles"
    }
  ]
}

