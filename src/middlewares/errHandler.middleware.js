const ApiError = require("../utils/errors.util")

const apiErrorHandler = (err,req,res,next) => {
    // dont use console in prod
    console.log(err)
    if(err instanceof ApiError){
        res.status(err.code).json({ error: err.message })
        return
    }
    res.status(500).json({ error: "something went wrong" })
}

module.exports = apiErrorHandler