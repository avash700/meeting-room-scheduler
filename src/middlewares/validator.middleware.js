const { check, validationResult } = require("express-validator")

const rolesRules = () => {
    return [
        check("role_type")
            .notEmpty()
            .trim()
            .withMessage("Role type cannot be empty!")
    ]
}

const userRules = () => {
    return [
        check('firstName')
            .isAlpha().withMessage("First name must only contain letters")
            .isLength({ max: 25 }).withMessage("Maximum length 25 required")
            .notEmpty()
            .trim()
            .withMessage('First name is required'),
        check('lastName')
            .isAlpha().withMessage("Last name must only contain letters")
            .isLength({ max: 25 }).withMessage("Maximum length 25 required")
            .notEmpty()
            .trim()
            .withMessage('Last name is required'),
        check('email')
            .trim()
            .isEmail()
            .withMessage('Valid email is required'),
        check('password')
            .trim()
            .isLength({ min: 8, max: 25 })
            .isStrongPassword()
            .withMessage('minimum length 8 with one uppercase one lowercase one number and one special character'),
        check('role_id')
            .isInt()
            .withMessage('Role id must be an integer'),
    ]
}

const changePasswordRules = () => {
    return [
        check('old_password')
            .notEmpty()
            .trim()
            .withMessage('old password is required'),
        check('new_password')
            .trim()
            .isLength({ min: 8, max: 25 })
            .isStrongPassword()
            .withMessage('minimum length 8 with one uppercase one lowercase one number and one special character')
    ]
}

const loginRules = () => {
    return [
        check('email')
            .trim()
            .isEmail()
            .withMessage('Valid email is required.'),
        check('password')
            .notEmpty()
            .trim()
            .withMessage('Password is required.'),
    ]
}

const forgotPasswordRules = () => {
    return [
        check('email')
            .trim()
            .isEmail()
            .withMessage('Valid email is required.')
    ]
}

const resetPasswordRules = () => {
    return [
        check('email')
            .trim()
            .isEmail()
            .withMessage('Valid email is required.'),
        check('password')
            .trim()
            .isLength({ min: 8, max: 25 })
            .isStrongPassword()
            .withMessage('minimum length 8 with one uppercase one lowercase one number and one special character'),
        check('otp')
            .isInt()
            .trim()
            .withMessage('otp must be a number')
    ]
}

const userUpdateRules = () => {
    return [
        check('firstName')
            .optional()
            .isAlpha().withMessage("First name must only contain letters")
            .isLength({ max: 25 }).withMessage("Maximum length 25 required")
            .notEmpty()
            .trim()
            .withMessage('First name cannot be empty.'),
        check('lastName')
            .optional()
            .isAlpha().withMessage("Last name must only contain letters")
            .isLength({ max: 25 }).withMessage("Maximum length 25 required")
            .notEmpty()
            .trim()
            .withMessage('Last name cannot be empty.'),
        check('email')
            .optional()
            .trim()
            .isEmail()
            .withMessage('Valid email is required and cannot be empty.')
    ]
}

const roomsRules = () => {
    return [
        check("room_name")
            .notEmpty()
            .trim()
            .withMessage("Room name cannot be empty!"),
        check("capacity")
            .isInt({ min: 1 })
            .trim()
            .withMessage("Capacity must be greater than 0"),
        check('amenities')
            .isArray()
            // .notEmpty()
            .withMessage('amenities array required'),
        check('amenities.*')
            .isInt()
            .withMessage('amenity id is required')
    ]
}

const roomsAmenitiesRules = () => {
    return [
        check('amenities')
            .isArray()
            .notEmpty()
            .withMessage('amenities array required'),
        check('amenities.*')
            .isInt()
            .withMessage('amenity id is required')
    ]
}

const updateRoomsRules = () => {
    return [
        check("room_name")
            .optional()
            .notEmpty()
            .trim()
            .withMessage("Room name cannot be empty!"),
        check("capacity")
            .optional()
            .isInt({ min: 1 })
            .trim()
            .withMessage("Capacity must be greater than 0"),
        check('amenities')
            .optional()
            .isArray()
            .notEmpty()
            .withMessage('amenities array required'),
        check('amenities.*')
            .optional()
            .isInt()
            .withMessage('amenity id is required')
    ]
}

const meetingRules = () => {
    return [
        check('room_id')
            .isInt()
            .trim()
            .withMessage('Room id is required.'),
        check('name')
            .notEmpty()
            .trim()
            .withMessage('Meeting name is required.'),
        check('description')
            .notEmpty()
            .trim()
            .withMessage('Meeting description is required.'),
        check('start')
            .isISO8601()
            .trim()
            .withMessage('Start date-time in format YYYY-MM-DDTHH:mm:ss.sss required'),
        check('end')
            .isISO8601()
            .trim()
            .withMessage('End date-time in format YYYY-MM-DDTHH:mm:ss.sss required'),
        check('attendees')
            .isArray()
            // .notEmpty()
            .withMessage('attendess array required'),
        check('attendees.*')
            .isInt()
            .withMessage('user id is required')
    ]
}

const updateMeetingRules = () => {
    return [
        check('room_id')
            .optional()
            .isInt()
            .trim()
            .withMessage('Room id is required.'),
        check('name')
            .optional()
            .notEmpty()
            .trim()
            .withMessage('Meeting name is required.'),
        check('description')
            .optional()
            .notEmpty()
            .trim()
            .withMessage('Meeting description is required.'),
        check('start')
            .optional()
            .isISO8601()
            .trim()
            .withMessage('Start date-time in format YYYY-MM-DDTHH:mm:ss.sss required'),
        check('end')
            .optional()
            .isISO8601()
            .trim()
            .withMessage('End date-time in format YYYY-MM-DDTHH:mm:ss.sss required'),
        check('attendees')
            .optional()
            .isArray()
            .notEmpty()
            .withMessage('attendess array required'),
        check('attendees.*')
            .optional()
            .isInt()
            .withMessage('user id is required')
    ]
}

const userMeetingRules = () => {
    return [
        check('attendees')
            .isArray()
            .notEmpty()
            .withMessage('attendess array required'),
        check('attendees.*')
            .isInt()
            .withMessage('user id is required')
    ]
}

const amenitiesRules = () => {
    return [
        check("amenity_name")
            .notEmpty()
            .trim()
            .withMessage("Amenity name cannot be empty!")
    ]
}

const validator = async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            return next()
        }
        const extractedErrors = []
        errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))
        return res.status(400).json({ error: extractedErrors })
    } catch (e) {
        console.log(e)
        res.status(500).json({ error: "internal server error" })
    }
}

const checkRoomAvailabe = () => {
    return [
        check('start')
            .isISO8601()
            .trim()
            .withMessage('Start date-time in format YYYY-MM-DDTHH:mm:ss.sss required'),
        check('end')
            .isISO8601()
            .trim()
            .withMessage('End date-time in format YYYY-MM-DDTHH:mm:ss.sss required')
    ]
}

module.exports = {
    rolesRules,
    userRules,
    userUpdateRules,
    loginRules,
    roomsRules,
    updateRoomsRules,
    meetingRules,
    userMeetingRules,
    amenitiesRules,
    updateMeetingRules,
    roomsAmenitiesRules,
    changePasswordRules,
    forgotPasswordRules,
    resetPasswordRules,
    checkRoomAvailabe,
    validator
}