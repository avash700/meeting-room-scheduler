const ApiError = require("../utils/errors.util")
const { validateToken } = require("../utils/jwt.util")

const checkAuth = (userRole = []) => {
    if (typeof (userRole) === "string") {
        userRole = [userRole]
    }

    // return another middleware
    return async (req, res, next) => {
        if (req.headers.authorization) {
            const token = req.headers.authorization.split(" ")[1];

            const decoded = await validateToken(token);
            if (decoded.error) return next(ApiError.notAuthorized(decoded.error))
            
            if (!userRole.length) return next(ApiError.notAuthorized("not authorized"))

            if(!userRole.includes(decoded.data.role_type)) return next(ApiError.notAuthorized("not authorized"))
            req.user = decoded.data
            return next();
        } else {
            next(ApiError.notAuthorized("no token provided"))
        }
    }
}

module.exports = {
    checkAuth
}