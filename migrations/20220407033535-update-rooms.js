'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('rooms', 'capacity', {
      type: Sequelize.INTEGER.UNSIGNED,
      allowNull: false
    })
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn('rooms', 'capacity')
  }
};
